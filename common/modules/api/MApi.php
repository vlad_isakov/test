<?php
namespace common\modules\api;

use yii\base\Module;

/**
 * Модуль API
 *
 * @package common\modules\api
 *
 * @author Исаков Владислав
 */
class MApi extends Module {
	public $controllerNamespace = 'common\modules\api\controllers';

	public function init() {
		parent::init();
	}
}