<?php
namespace common\modules\api\components;

use yii\base\Component;

/**
 * Базов
 *
 * @package common\components
 *
 * @author  Исаков Владислав
 */
class BaseResponseApi {
	/** @var string Статус ответа */
	public $status = 'Success';

	/** @var string Сообщение ответа */
	public $message = 'Успешно';

	/** @var array Данные ответа */
	public $data;
}