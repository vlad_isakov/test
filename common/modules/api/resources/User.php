<?php
namespace common\modules\api\resources;

/**
 *
 * @author  Исаков Владислав
 */
class User {
	/** @var int */
	public $id;

	/** @var string */
	public $firstName;

	/** @var string */
	public $secondName;

}