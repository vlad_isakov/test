<?php
/**
 * @author Исаков Владислав
 */

namespace common\modules\api\exceptions;


use yii\web\BadRequestHttpException;

class MissingParamException extends BadRequestHttpException {

}