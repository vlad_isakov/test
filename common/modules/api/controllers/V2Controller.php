<?php

namespace common\modules\api\controllers;

use common\modules\api\components\ApiController;
use common\modules\api\components\BaseResponseApi;
use common\modules\api\resources\Place;
use common\modules\api\resources\Post;
use common\modules\api\resources\User;
use Exception;
use StdClass;
use Yii;
use yii\caching\TagDependency;
use yii\filters\VerbFilter;
use yii\mongodb\Query as MongoQuery;
use yii\web\Response;

/**
 * Контроллер методов API v2
 *
 * @author Исаков Владислав
 */
class V2Controller extends ApiController {

	/**
	 * @inheritDoc
	 *
	 * @author Исаков Владислав
	 */
	public function behaviors() {
		return [
			'verbs' => [
				'class'   => VerbFilter::class,
				'actions' => [
					'posts' => ['GET'],
					'error' => ['GET'],
				],
			],
		];
	}

	/**
	 * @param int    $limit
	 * @param int    $offset
	 * @param string $userId
	 *
	 * @return BaseResponseApi
	 *
	 * @author Исаков Владислав
	 */
	public function actionPosts(string $userId, int $limit = 20, int $offset = 0): BaseResponseApi {
		Yii::$app->response->format = Response::FORMAT_JSON;
		$response = new BaseResponseApi();

		try {
			$query = new MongoQuery();

			$posts = $query->select([])
				->from('posts')
				->where(['userId' => $userId])
				->limit($limit)
				->offset($offset)
				->all();

			if (count($posts) === 0) {
				$response->status = 'RecordNotFound';
				$response->message = 'Запись не найдена';
				$response->data = new StdClass();

				return $response;
			}

			$resPosts = [];

			foreach ($posts as $post) {
				$resPost = new Post();
				$resPost->id = (string)$post['_id'];

				$cacheKey = Yii::$app->cache->buildKey([User::class, $post['userId']]);
				$resUser = Yii::$app->cache->get($cacheKey);

				if (false === $resUser) {
					$resUser = new User();
					$mongoUser = $query->select([])
						->from('users')
						->where(['_id' => $post['userId']])
						->one();

					$resUser->id = (string)$mongoUser['_id'];
					$resUser->firstName = $mongoUser['firstName'];
					$resUser->secondName = $mongoUser['secondName'];

					Yii::$app->cache->set($cacheKey, $resUser, null, new TagDependency(['tags' => User::class]));
				}

				$resPost->user = $resUser;

				$cacheKey = Yii::$app->cache->buildKey([Place::class, $post['placeId'], 1]);
				$resPlace = Yii::$app->cache->get($cacheKey);

				if (false === $resPlace) {
					$resPlace = new Place();
					$mongoPlace = $query->select([])
						->from('places')
						->where(['_id' => $post['placeId']])
						->one();

					$resPlace->id = (string)$mongoPlace['_id'];
					$resPlace->name = $mongoPlace['name'];
					$resPlace->city = $mongoPlace['city'];
					$resPlace->street = $mongoPlace['street'];

					Yii::$app->cache->set($cacheKey, $resPlace, null, new TagDependency(['tags' => Place::class]));
				}

				$resPost->place = $resPlace;
				$resPost->text = $post['text'];
				$resPost->timePassed = time() - (int)$post['createdAt'];

				$resPosts[] = $resPost;
			}

			$postsToResponse = new StdClass();
			$postsToResponse->posts = $resPosts;

			$response->data = $postsToResponse;
		}
		catch (Exception $e) {
			$response->status = 'GeneralInternalError';
			$response->message = 'Произошла ошибка: ' . $e->getMessage();
			$response->data = new StdClass();

			return $response;
		}

		return $response;
	}
}