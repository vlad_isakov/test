<?php
/**
 * @author Исаков Владислав
 */

namespace common\modules\api\exceptions;


use yii\web\BadRequestHttpException;

class InvalidParamTypeException extends BadRequestHttpException {

}