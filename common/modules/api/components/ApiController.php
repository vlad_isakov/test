<?php

namespace common\modules\api\components;

use common\modules\api\exceptions\InvalidParamTypeException;
use common\modules\api\exceptions\MissingParamException;
use ReflectionMethod;
use StdClass;
use Yii;
use yii\base\InlineAction;
use yii\rest\Controller;
use yii\web\NotFoundHttpException;
use yii\web\Response;

/**
 * Базовый контроллер API v2
 *
 * @author Исаков Владислав
 */
class ApiController extends Controller {

	/**
	 * @param \yii\base\Action $action
	 * @param array            $params
	 *
	 * @return array
	 *
	 * @throws \ReflectionException
	 * @throws \common\modules\api\exceptions\InvalidParamTypeException
	 * @throws \common\modules\api\exceptions\MissingParamException
	 *
	 * @author Исаков Владислав
	 */
	public function bindActionParams($action, $params) {
		if ($action instanceof InlineAction) {
			$method = new ReflectionMethod($this, $action->actionMethod);
		}
		else {
			$method = new ReflectionMethod($action, 'run');
		}

		$args = [];
		$missing = [];
		$actionParams = [];
		foreach ($method->getParameters() as $param) {
			$name = $param->getName();
			if (array_key_exists($name, $params)) {
				$isValid = true;
				if ($param->isArray()) {
					$params[$name] = (array)$params[$name];
				}
				elseif (is_array($params[$name])) {
					$isValid = false;
				}
				elseif (
					PHP_VERSION_ID >= 70000 &&
					($type = $param->getType()) !== null &&
					$type->isBuiltin() &&
					($params[$name] !== null || !$type->allowsNull())
				) {
					$typeName = PHP_VERSION_ID >= 70100 ? $type->getName() : (string)$type;
					switch ($typeName) {
						case 'int':
							$params[$name] = filter_var($params[$name], FILTER_VALIDATE_INT, FILTER_NULL_ON_FAILURE);
							break;
						case 'float':
							$params[$name] = filter_var($params[$name], FILTER_VALIDATE_FLOAT, FILTER_NULL_ON_FAILURE);
							break;
					}
					if ($params[$name] === null) {
						$isValid = false;
					}
				}
				if (!$isValid) {
					throw new InvalidParamTypeException($name);
				}
				$args[] = $actionParams[$name] = $params[$name];
				unset($params[$name]);
			}
			elseif ($param->isDefaultValueAvailable()) {
				$args[] = $actionParams[$name] = $param->getDefaultValue();
			}
			else {
				$missing[] = $name;
			}
		}

		if (!empty($missing)) {
			throw new MissingParamException(implode(', ', $missing));
		}

		$this->actionParams = $actionParams;

		return $args;
	}

	/**
	 * Вывод ошибок
	 *
	 * @return BaseResponseApi
	 *
	 * @author Исаков Владислав
	 */
	public function actionError(): BaseResponseApi {
		Yii::$app->response->format = Response::FORMAT_JSON;
		$exception = Yii::$app->errorHandler->exception;

		$response = new BaseResponseApi();

		if ($exception !== null) {
			$message = $exception->getMessage();

			$exceptionClass = get_class($exception);
			switch ($exceptionClass) {
				case MissingParamException::class:
					$response->status = 'FieldRequired';
					$response->message = 'Поле не может быть пустым';
					$fields = new StdClass();
					$fields->fields = explode(',', str_replace(' ', '', $message));
					$response->data = $fields;
					break;
				case InvalidParamTypeException::class:
					$response->status = 'FieldInvalid';
					$response->message = 'Поле содержит недопустимое значение';
					$fields = new StdClass();
					$fields->fields = $message;
					$response->data = $fields;
					break;
				case NotFoundHttpException::class:
					$response->status = 'UrlNotFound';
					$response->message = 'URL не найден';
					$response->data = new StdClass();
					break;
				default:
					break;
			}

			return $response;
		}
	}
}