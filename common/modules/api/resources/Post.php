<?php

namespace common\modules\api\resources;

/**
 * Класс ресурса Post
 *
 * @package common\modules\api\resources
 *
 * @author  Исаков Владислав
 */
class Post {
	/** @var int */
	public $id;

	/** @var \common\modules\api\resources\User */
	public $user;

	/** @var \common\modules\api\resources\Place */
	public $place;

	/** @var string */
	public $text;

	/** @var int */
	public $timePassed;
}