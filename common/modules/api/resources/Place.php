<?php

namespace common\modules\api\resources;

/**
 * @author  Исаков Владислав
 */
class Place {
	/** @var string */
	public $id;
	/** @var string */
	public $name;
	/** @var string */
	public $city;
	/** @var string */
	public $street;
	/** @var string */
	public $category;
}